﻿using UnityEngine;
using System.Collections;

public class CrownOfThornsIncrease : MonoBehaviour {

	public int waitBetweenAdd = 5;

	private bool isAlreadyRunning = false;
	private IEnumerator coroutine;

	// Use this for initialization
	void Start () {
		removeAllChildren();
	}
	
	// Update is called once per frame
	void Update () {
		// Slowly add more starfish
		if (Input.GetKeyUp(KeyCode.X) && !isAlreadyRunning) {
			isAlreadyRunning = true;
			coroutine = childrenAdder(waitBetweenAdd);
			StartCoroutine(coroutine);
		}

		// Reset the scene
		if (Input.GetKeyUp(KeyCode.R)) {
			if (isAlreadyRunning)
				StopCoroutine(coroutine);
			
			isAlreadyRunning = false;
			removeAllChildren();
		}
	}
	void removeAllChildren() {
		for (int i = 0; i < transform.childCount; i++) {
			transform.GetChild(i).gameObject.SetActiveRecursively(false);
		}
	}

	IEnumerator childrenAdder(int waitBetweenEach)
	{
		for (int i = 0; i < transform.childCount; i++) {
			transform.GetChild(i).gameObject.SetActiveRecursively(true);
			yield return new WaitForSeconds(waitBetweenEach);
		}

		isAlreadyRunning = false;
	}
}
