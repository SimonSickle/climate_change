/* 
 * Last edit: 10-29-15
 * By: Simon Sickle
 * reason: Fixed caustic speed on new unity
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CausticsController : MonoBehaviour {

	public float fps = 12.0f;
	public Texture2D[] textures;

	private int frame = 1;
	private Projector projector;

	void Start () {
		TextureCompare textComp = new TextureCompare();
		Array.Sort (textures, textComp);
		projector = GetComponent<Projector> ();
		NextFrame();
		InvokeRepeating("NextFrame", 1 / fps, 1 / fps);
	
	}

	void NextFrame() {
		projector.material.SetTexture ("_ShadowTex", textures[frame]);
		frame = (frame + 1) % textures.Length;
	}

	public class TextureCompare : IComparer<Texture2D> {
		public int Compare (Texture2D left, Texture2D right)
		{
			return left.name.CompareTo (right.name);
		}
	}
}