﻿/* 
 * Last edit: 10-29-15
 * By: Simon Sickle
 * reason: change colors in a different way
 */

using UnityEngine;
using System.Collections;

public class coralBleaching : MonoBehaviour {
	// Reference: http://docs.unity3d.com/ScriptReference/Material-color.html
	// Simon Sickle: 11/16/15

	public float duration = 5f;

	private Color32[] colorStart;
	private Renderer rend;
	private bool isBleached = false;
	private bool isCoRun = false;
	private IEnumerator coroutine;

	void Start () {
		// Set the renderer
		rend = GetComponent<Renderer> ();
		colorStart = new Color32[rend.materials.Length];

		// Set the starting color
		for (int i = 0; i < rend.materials.Length; i++) {
			colorStart [i] = rend.materials [i].color;
		}
	}
	
	// Update is called once per frame
	void Update () {
		// Check if z key is pressed. Also verify color is not currently changing
		if (Input.GetKeyUp (KeyCode.Z) && !isCoRun) {
			// Loop through all materials
			for (int i = 0; i < rend.materials.Length; i++) {
				if (isBleached)
				{
					isCoRun = true;
					coroutine = changeColor(rend.materials [i].color, colorStart[i], i);
					StartCoroutine(coroutine);
				} else {
					isCoRun = true;
					coroutine = changeColor(rend.materials [i].color, Color.white, i);
					StartCoroutine(coroutine);
				}

				// All colors are bleached once reach i + 1 being the length
				if (i + 1 == rend.materials.Length)
					isBleached = !isBleached;
			}
		}
		if (Input.GetKeyUp (KeyCode.R)) {
			isBleached = false;

			if (isCoRun)
				StopCoroutine(coroutine);
			
			isCoRun = false;
			for (int i = 0; i < rend.materials.Length; i++) {
				rend.materials [i].color = colorStart [i];
			}
		}
	}

	// Change the color with lerp
	IEnumerator changeColor(Color startColor, Color endColor, int index)
	{
		float currentTime = 0f;
		float time = 0f;

		// Time must be < 1 for duration (in seconds)
		while (time < 1) {
			// Set time as the percent (< 1)
			time = currentTime / duration;
			// Add deltaTime to account for frames
			currentTime += Time.deltaTime;
			// Change the color
			rend.materials [index].color = Color32.Lerp (startColor, endColor, time);
			yield return new WaitForEndOfFrame ();
		}

		// Thread is no longer running in the background
		isCoRun = false;
	}
}