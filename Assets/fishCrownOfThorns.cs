﻿using UnityEngine;
using System.Collections;

public class fishCrownOfThorns : MonoBehaviour {

	public int waitBetweenAdd = 5;

	private bool isAlreadyRunning = false;
	private IEnumerator coroutine;

	// Update is called once per frame
	void Update () {
		// Slowly add more starfish
		if (Input.GetKeyUp(KeyCode.X) && !isAlreadyRunning) {
			isAlreadyRunning = true;
			coroutine = childrenAdder(waitBetweenAdd);
			StartCoroutine(coroutine);
		}

		// Reset the scene
		if (Input.GetKeyUp(KeyCode.R)) {
			if (isAlreadyRunning)
				StopCoroutine(coroutine);

			isAlreadyRunning = false;
			addAllChildren();
		}
	}

	void addAllChildren() {
		for (int i = 0; i < transform.childCount; i++) {
			transform.GetChild(i).gameObject.SetActiveRecursively(true);
		}
	}

	IEnumerator childrenAdder(int waitBetweenEach)
	{
		for (int i = 0; i < transform.childCount; i++) {
			transform.GetChild(i).gameObject.SetActiveRecursively(false);
			yield return new WaitForSeconds(waitBetweenEach);
		}

		isAlreadyRunning = false;
	}
}