﻿/* 
 * 
 * Last edit: 11-24-15
 * By: Simon Sickle
 * Added debugging output for future
 * 
 */

using UnityEngine;
using System.Collections;

public class iTweenMover : MonoBehaviour {

	public string pathName = string.Empty; // Name of the iTween path
	public float timeToMove = 0f; // How long it should take to navigate the path
	public bool orientationToPath = true; // Will rotate with the path
	public float lookTime = 0; // How long it takes to snap to the path
	public float delay = 0; // How long to start moving / restart loop
	public bool loop = true; // Loop or end after 1 move
	
	private GameObject character = null; // game object to be moved

	void Start () {
		character = this.gameObject; // Set the game object to be manipulated (the obj this script is attatched to)

		// Validate game object
		if (character == null) {
			Debug.LogError ("**iTweenMover.cs** ERROR: Game object is not defined" +
			                "Did accidently attatch this script to a non game object?");
			return;
		}

		// Validate path name
		if (pathName == null || pathName == string.Empty) {
			Debug.LogError("ERROR: Path name is NULL/empty for game object:  " + character.ToString());
			return;
		}

		// Initalize with iTween library to prevent quirks
		iTween.Init (character);
		Move ();
	}

	void Move() {
		// Set to loop by default
		var loopType = iTween.LoopType.loop;

		// Don't loop at all if loop is set to false
		if (!loop)
			loopType = iTween.LoopType.none;

		// Move with iTween library
		// Passes game object and an iTween hash
		// the hash is documented at http://itween.pixelplacement.com/documentation.php
		iTween.MoveTo (character, iTween.Hash ("delay", delay, "path", iTweenPath.GetPath (pathName),
		                                       "orienttopath", orientationToPath, "looktime", lookTime, 
		                                       "time", timeToMove, "loopType", loopType,
		                                       "easeType", iTween.EaseType.linear));
	}
}